<?php
/***
*		Basic PHP Loadingscreen created by White_star
*		Visit originalwhitestar.net/products for more information.
*		Colors and design used from Flat-UI FREE.
*/

//Steam API Key - Register one here(steamcommunity.com/dev/apikey):
$apikey = '808D19BA8BF6BE5A1C4E627E65435EF8';

//Do not edit the PHP code below this line.

////////////////////////////////////////////////////////////

//If not setup properly
if (!isset($_GET['ID'])) {
	die('Are you sure you set up your loadingscreen properly? Be sure to set it like "yourdomain.tld/loadingscreen?ID=%s&HEADER=SERVERNAME"!');
}
if (!isset($_GET['HEADER'])) {
	die('Are you sure you set up your loadingscreen properly? Be sure to set it like "yourdomain.tld/loadingscreen?ID=%s&HEADER=SERVERNAME"!');
}

//Get connecting player info
$sid64 = $_GET["ID"];
$header = $_GET["HEADER"];
$authserver = bcsub($sid64, '76561197960265728') & 1;
$authid = (bcsub($sid64, '76561197960265728')-$authserver)/2;
$steamid = "STEAM_0:$authserver:$authid";
$xml = simplexml_load_file('http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key='.$apikey.'&steamids='.$sid64.'&format=xml');
$steamname = $xml->players->player->personaname;
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="Welcome to SERVERNAME servers" />
	<title>Loadingscreen :: <?php echo $header;?></title>

	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="//data.originalwhitestar.net/css/flat-ui.css">
	<link rel="stylesheet" href="//data.originalwhitestar.net/css/custom.css">
	<link rel="stylesheet" href="style.css">
	<style type='text/css'>
	.centered{
		left:50%;
		text-align:center;max-width:90%;
	}
	</style>
</head>
<body style='background-color:#34495E;'>
	<div class="container centered" style="background-color:#34495E;">

		<div class="center" style='background-color:#1A242E;width:100%'>
			<h2>Welcome to <?php echo $header ?></h2>
		</div>

		<div class='left' style='background-color:#1A242E;padding:15px;'>
			<h2>Serverinfo</h2>
			<b>Server name:</b><span id="s-name"> SERVER NAME</span><br>
			<b>Server map:</b><span id="s-map"> SERVER MAP</span><br>
			<b>Server gamemode:</b><span id="s-mode"> SERVER GAMEMODE</span>
		</div>

		<div class='right' style='background-color:#1A242E;padding:15px;'>
			<h2>Player info</h2>
			<b>Player name:</b><span> <?php echo $steamname?></span><br>
			<b>Player SteamID:</b><span> <?php echo $steamid;?></span><br>
			<span><u>Copyright &copy; originalwhitestar.net</u></span>
		</div>

		<div class="center">
			<?php
			if ($_GET["TRACKER"] == "true") {
				if (!isset($_GET["IP"]) or !isset($_GET["PORT"])) {
					die("Gametracker have not been setup properly.");
				} else {
					echo '<img style="box-shadow:0px 0px 15px #000;" src="http://cache.www.gametracker.com/server_info/' . $_GET["IP"] . ':' . $_GET["PORT"] . '/b_560_95_1.png" border="0" width="560" height="95" alt=""/>';
				}
			}
			 ?>
		</div>
	</div>
	
	<script type="text/javascript" src="js/main.js"></script>
</body>
</html>